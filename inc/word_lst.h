/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   word_lst.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/09 08:03:55 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/02/09 12:06:29 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WORD_LST_H
# define WORD_LST_H

typedef struct s_lst
{
	char			*content;
	int				dis;
	struct s_lst	*next;
	struct s_lst	*prev;
}	t_lst;

t_lst	*init_node(char *input, int size);
void	delete_node(t_lst *node);
void	unlink_node(t_lst *node);
void	add_node_front(t_lst *node, t_lst **lst);
void	add_node_back(t_lst *node, t_lst **lst);

#endif