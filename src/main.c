/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/11 16:30:40 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/02/10 12:35:01 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	free_wordlst(t_lst *lst)
{
	t_lst	*temp;

	while (lst)
	{
		temp = lst->next;
		delete_node(lst);
		lst = temp;
	}
}

int	error_print(char *error_msg, t_lst *lst)
{
	if (lst)
		free_wordlst(lst);
	ft_putstr_fd(error_msg, 2);
	return (1);
}

int	add_seperator(char *input, t_lst *lst)
{
	int size;

	size = 0;
	if ((input[size] == '<' || input[size] == '>')
		&& input[size] == input[size + 1])
		size++;
	if (!make_token(input, size, lst))
		return (1);
	return (0);
}

int	make_token(char *input, int index, t_lst *lst)
{
	t_lst	*node;

	node = init_node(input, index + 1);
	if (!node)
		return (1);
	add_node_back(node, &lst);
	ft_memmove(input, &input[index], ft_strlen(&input[index]));
	return (0);
}

int	tokenize(char *input, t_lst *lst)
{
	int		i;
	char	*flag;

	i = 0;
	flag = NULL;
	while (input[i] != '\0')
	{
		if (flag && *flag == *ft_strchr("'\"", input[i]))
			flag == NULL;
		else
			flag = ft_strchr("'\"", input[i]);
		if (!flag && ft_strchr("| ><", input[i]))
			if (!(make_token(input, i, lst) == 0
					&& add_seperator(input, lst) == 0))
				return (1);
			else
				i = -1;
		i++;
	}
	return (0);
}

void	clear_empty(t_lst *lst)
{
	t_lst	*temp;

	temp = lst;
	while (lst)
	{
		ft_strtrim(temp->content, " ");
		if (ft_strlen(temp->content) == 0)
		{
			if (temp == lst)
				lst = lst->next;
			temp = temp->next;
			delete_node(temp->prev);
		}
		else
			temp = temp->next;
	}
}

char	*ft_replace(char *src, int index, int size, char *replace)
{
	int		length;
	char	*result;

	length = ft_strlen(replace);
	printf("%d\n", length);
	result = malloc(sizeof(char) * (ft_strlen(src) - size + length + 1));
	if (!result)
		return (NULL);
	ft_memcpy(result, src, index);
	ft_memcpy(&result[index], replace, length);
	ft_memcpy(&result [index + length], &src[index + size],
		ft_strlen(&src[index + size]));
	free(src);
	return (result);
}

char *expansion(char *str)
{
	char	*temp;
	char	*var_name;
	int		i;

	i = ft_strlen(str);
	preprocess(str, '$');
	i = ft_strrchr(str, '$');
	while (str[i])
	{
		if (str[i] == '_' || ft_isalnum(str[i]))
		{
			temp = expansion(&str[i + 1]);
			var_name = substr(str, i, get_word_size(str[i]));
			if (!var_name)
				return (NULL);
			result = ft_replace(temp, i, ft_strlen(var_name), get_env(var_name));
			i = ft_strchr(str, '$');
		}
		i--;
	}
	preprocess(str, -1 * '$');
}

void	remove_quotes(void *list)
{
	t_lst	*lst;
	int		i;
	int		first;

	i = 0;
	first = -1;
	lst = (t_lst *)list;
	while (lst->content[i])
	{
		if (ft_strchr("'\"", lst->content[i]))
		{
			if (first == -1)
				first = i;
			if (first != i && lst->content[i] == lst->content[first])
			{
				ft_memmove(&lst->content[i], &lst->content[i + 1],
					ft_strlen(&lst->content[i + 1]));
				ft_memmove(&lst->content[first], &lst->content[first + 1],
					t_strlen(&lst->content[first + 1]));
				first = -1;
				i--;
			}
		}
	}
}

int		check_qoutes(char *str)
{
	int		i;
	int		err;
	char	*flag;

	i = 0;
	err = 0;
	flag = NULL;
	while (str[i])
	{
		if (flag && *flag == *ft_strchr("'\"", str[i]))
		{
			err = 0;
			flag == NULL;
		}
		else
		{
			err = 1;
			flag = ft_strchr("'\"", str[i]);
		}
		i++;
	}
	return (err);
}

t_lst	*parsing(char *str)
{
	t_lst	*lst;
	char	*input;

	if (!check_qoutes(str))
		return (error_print("Error: Unclosed qutation marks.\n", lst));
	input = ft_strdup(str);
	if (!input)
		return (error_print("Memory error\n", lst));
	lst = NULL;
	if (tokenize(input, lst))
		return (error_print("Memory error\n", lst));
	clear_empty(lst);
	if (expansions(lst))
		return (NULL);
	// set_lst_bools(lst, pars);
	free(input);
	return (lst);
}

int	main(int argc, char **argv)
{
	t_lst	*list;

	if (argc == 1)
		return (printf("input argument\n"), 1);
	list = parsing(argv[1]);
	if (!list)
		return (1);
	while (list)
	{
		printf("Node : (%s)\n", list->content);
		list = list->next;
	}
	return (0);
}
