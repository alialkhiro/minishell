/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/20 08:27:40 by aalkhiro          #+#    #+#             */
/*   Updated: 2021/12/27 15:24:29 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t	i;
	size_t	size1;
	size_t	size2;
	char	*dest;

	if (!s1 || !s2)
		return (NULL);
	i = 0;
	size1 = ft_strlen(s1);
	size2 = ft_strlen(s2);
	dest = (char *)malloc(size1 + size2 + 1);
	if (!dest)
		return (0);
	while (i <= size1)
	{
		dest[i] = s1[i];
		i++;
	}
	i--;
	while (i <= size1 + size2)
	{
		dest[i] = s2[i - size1];
		i++;
	}
	return (dest);
}
