/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/20 07:50:53 by aalkhiro          #+#    #+#             */
/*   Updated: 2021/12/27 16:36:25 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t	i;
	char	*ss;

	ss = (char *) s;
	i = 0;
	while (i < n)
	{
		if (ss[i] == (char)c)
			return ((char *)ss + i);
		i++;
	}
	return (0);
}
