/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/20 09:12:56 by aalkhiro          #+#    #+#             */
/*   Updated: 2021/12/27 09:52:15 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	i;
	size_t	j;
	size_t	srchln;
	char	*ss1;

	ss1 = (char *)s1;
	srchln = ft_strlen(s2);
	i = 0;
	if (srchln == 0 || s1 == s2)
		return (ss1);
	while (ss1[i] != '\0' && i < n)
	{
		if (ss1[i] == s2[0])
		{
			j = 0;
			while (ss1[i + j] != '\0' && s2[j] != '\0'
				&& s1[i + j] == s2[j] && i + j < n)
				j++;
			if (j == srchln)
				return (ss1 + i);
		}
		i++;
	}
	return (0);
}
