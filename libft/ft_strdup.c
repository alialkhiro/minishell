/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/20 08:23:14 by aalkhiro          #+#    #+#             */
/*   Updated: 2022/09/21 08:16:41 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s)
{
	size_t		size;
	char		*dest;

	size = ft_strlen(s) + 1;
	dest = (char *)malloc(sizeof(char) * size);
	if (!dest)
		return (0);
	dest = ft_memcpy(dest, s, size);
	return (dest);
}
