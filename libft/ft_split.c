/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/14 15:35:13 by aalkhiro          #+#    #+#             */
/*   Updated: 2022/09/21 16:48:08 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	*clear_str_arr(char **arr)
{
	int	i;

	i = 0;
	if (arr)
		while (arr[i])
			free(arr[i++]);
	free(arr);
	return (NULL);
}

static int	ft_countstr(char const *str, char c)
{
	int	i;
	int	count;
	int	f;

	f = 1;
	count = 0;
	i = 0;
	while (str[i])
	{
		if (str[i] != c && f)
		{
			f = 0;
			count++;
		}
		if (str[i] == c && !f)
			f = 1;
		i++;
	}
	return (count);
}

static char	*ft_getstr(int start, int end, char const *str)
{
	int		i;
	char	*temp;

	if (start == end)
		return (NULL);
	temp = malloc(sizeof(char) * end - start + 1);
	if (!temp)
		return (NULL);
	i = 0;
	while (i + start < end)
	{
		temp[i] = str[i + start];
		i++;
	}
	temp[i] = '\0';
	return (temp);
}

static int	findcpy(char const *str, char c, int start, int mod)
{
	int	j;

	j = start;
	while (str[j])
	{
		if (mod)
		{
			if (str[j] == c)
				return (j);
		}
		else
		{
			if (str[j] != c)
				return (j);
		}
		j++;
	}
	return (j);
}

char	**ft_split(char const *s, char c)
{
	char	**splt;
	int		i;
	int		ind;
	int		j;

	if (!s)
		return (NULL);
	ind = 0;
	splt = malloc(sizeof(char *) * (ft_countstr(s, c) + 1));
	if (!(splt))
		return (NULL);
	i = 0;
	while (s[i])
	{
		i = findcpy(s, c, i, 0);
		j = findcpy(s, c, i, 1);
		if (i == j)
			break ;
		splt[ind++] = ft_getstr(i, j, s);
		if (!splt[ind - 1])
			return (clear_str_arr(splt));
		i = j;
	}
	splt[ind] = 0;
	return (splt);
}
