/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/20 09:53:01 by aalkhiro          #+#    #+#             */
/*   Updated: 2021/12/27 10:56:58 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s1, char const *set)
{
	int		i;
	int		j;
	size_t	setlen;
	char	*trimed;

	if (!s1)
		return (NULL);
	if (!set)
		return (ft_strdup(s1));
	setlen = ft_strlen(set);
	i = 0;
	j = ft_strlen(s1);
	while (i < j && ft_memchr(set, s1[i], setlen))
	{
		i++;
	}
	while (ft_memchr(set, s1[j - 1], setlen) && j > i)
	{
		j--;
	}
	trimed = ft_substr(s1, i, j - i);
	return (trimed);
}
