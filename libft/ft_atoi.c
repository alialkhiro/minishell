/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/06 19:22:31 by aalkhiro          #+#    #+#             */
/*   Updated: 2021/12/27 15:47:36 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_isspace(char c)
{
	if (c == '\t'
		|| c == '\n'
		|| c == '\v'
		|| c == '\f'
		|| c == '\r'
		|| c == 32)
		return (1);
	else
		return (0);
}

int	ft_atoi(const char *str)
{
	int		i;
	int		m;
	long	sum;

	i = 0;
	m = 1;
	sum = 0;
	while (str[i] != '\0' && ft_isspace(str[i]))
		i++;
	if (str[i] != '\0' && (str[i] == '-' || str[i] == '+'))
	{
		if (str[i] == '-')
			m = -1;
		++i;
	}
	while (str[i] != '\0' && ft_isdigit(str[i]))
	{
		sum = sum * 10 + (str[i] - '0');
		++i;
		if (sum * m > 2147483647)
			return (-1);
		if (sum * m < -2147483648)
			return (0);
	}
	return (sum * m);
}
