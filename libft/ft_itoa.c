/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/20 07:34:54 by aalkhiro          #+#    #+#             */
/*   Updated: 2022/09/21 08:14:54 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_size(long n)
{
	int	i;

	if (n == 0)
		return (1);
	i = 0;
	while (n > 0)
	{
		n /= 10;
		i++;
	}
	return (i);
}

static char	*ft_conv(int size, int sign, char *num, long nbr)
{
	num[size + sign] = '\0';
	while (size + sign > 0)
	{
		num[size + sign - 1] = (nbr % 10) + '0';
		nbr = nbr / 10;
		size--;
	}
	return (num);
}

char	*ft_itoa(int n)
{
	char	*num;
	int		sign;
	int		size;
	long	nbr;

	sign = 0;
	nbr = n;
	if (nbr < 0)
	{
		sign = 1;
		nbr = -nbr;
	}
	size = ft_size(nbr);
	num = (char *)malloc(sizeof(char) * (size + sign + 1));
	if (!num)
		return (0);
	num = ft_conv(size, sign, num, nbr);
	if (sign)
		num[0] = '-';
	return (num);
}
