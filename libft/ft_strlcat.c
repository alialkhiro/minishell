/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/06 11:20:58 by aalkhiro          #+#    #+#             */
/*   Updated: 2021/12/20 10:41:36 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dest, const char *src, size_t size)
{
	size_t	out;
	size_t	i;

	i = 0;
	out = ft_strlen(dest);
	if (size == '0')
		return (ft_strlen(src));
	if (size > out)
	{
		while (src[i] && i < (size - out - 1))
		{
			dest[out + i] = src[i];
			i++;
		}
		dest[out + i] = '\0';
	}
	if (size > out)
		out += ft_strlen(src);
	else
		out = size + ft_strlen(src);
	return (out);
}
