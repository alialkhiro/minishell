# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/10/20 09:52:56 by aalkhiro          #+#    #+#              #
#    Updated: 2022/10/20 10:39:24 by aalkhiro         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	=	minishell
NAMEB	=	minishell_bonus
CC		=	gcc
CFLAGS	=	-Werror -Wextra -Wall
CFLAGS	+=	-I inc
CFLAGS	+=	-I $(LFTD)
LFLAGS	=	-L $(LFTD) -lft -lreadline
LFTD	=	libft/
LFT		=	libft.a
OBJ		=	$(patsubst src%, obj%, $(SRC:.c=.o))
OBJB	=	$(patsubst src%, obj%, $(SRCB:.c=.o))
SRC		=	$(addprefix src/,	\
				main.c			\
			)
SRCB	=	$(addprefix src/,	\
				main_bonus.c	\
			)

all:		${NAME}

bonus:		${NAMEB}

$(NAME):	$(LFTD)$(LFT) $(OBJ) 
		$(CC) $(OBJ) $(LFLAGS) -o $@

$(LFTD)$(LFT):
		@make -s -C $(LFTD)

$(NAMEB):	$(LFTD)$(LFT) ${OBJB} 
		$(CC) ${OBJB} $(LFLAGS) -o $@

obj/%.o:	src/%.c
		@mkdir -p obj
		$(CC) $(CFLAGS) $(INC) -o $@ -c $<

clean:
		@make -s $@ -C libft
		@rm -rf $(OBJ) obj
		@echo "object files removed."

fclean: 	clean
		@make -s $@ -C libft
		@rm -rf $(NAME) $(NAMEB)
		@echo "object & binary files removed."

re: 		fclean all

.PHONY:		all bonus clean fclean re
